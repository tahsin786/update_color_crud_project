<x-backend.layouts.master>
    <x-slot name="pageTitle">
        Colors
    </x-slot>

    <x-slot name='breadCrumb'>
        <x-backend.layouts.elements.breadcrumb>
            <x-slot name="pageHeader">Colors</x-slot>

            <li class="breadcrumb-item"><a href="index.html">Dashboard</a></li>
            <li class="breadcrumb-item active">Colors</li>

        </x-backend.layouts.elements.breadcrumb>
    </x-slot>

    <div class="card mb-4">
        <div class="card-header">
            <i class="fas fa-table me-1"></i>
            Colors <a class="btn btn-sm btn-info" href="{{ route('colors.create') }}">Add New</a>
        </div>
        <div class="card-body">

            @if (session('message'))
            <div class="alert alert-success">
                <span class="close" data-dismiss="alert">&times;</span>
                <strong>{{ session('message') }}.</strong>
            </div>
            @endif

            <table id="datatablesSimple">
                <thead>
                    <tr>
                        <th>Sl#</th>
                        <th>Title</th>
                        <th>Description</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    @php $sl=0 @endphp
                    @foreach ($colors as $color)
                    <tr>
                        <td>{{ ++$sl }}</td>
                        <td>{{ $color->title }}</td>
            
                        <td>
                            <a class="btn btn-info btn-sm" href="{{ route('colors.show', ['color' => $color->id]) }}" >Show</a>

                            <a class="btn btn-warning btn-sm" href="{{ route('colors.edit', ['color' => $color->id]) }}" >Edit</a>

                            <form style="display:inline" action="{{ route('colors.destroy', ['color' => $color->id]) }}" method="post">
                                @csrf
                                @method('delete')
                                
                                <button onclick="return confirm('Are you sure want to delete ?')" class="btn btn-sm btn-danger" type="submit">Delete</button>
                            </form>



                        </td>
                    </tr>
                    @endforeach

                </tbody>
            </table>
        </div>
    </div>

</x-backend.layouts.master>